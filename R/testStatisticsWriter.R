source("R/utils.R")

test1 = readTestData("acnetreatmentdesigns", "original", "design1")
test2 = readTestData("acnetreatmentdesigns", "original", "design2")
tests = list(test1, test2)

tests[[3]] = readTestData("moleremoval", "original", "moleredesign")

tests[[4]] = readTestData("bookingpagedesign", "original", "design1")
tests[[5]] = readTestData("bookingpagedesign", "original", "design2")

tests[[6]] = readTestData("PostcodeAndRegionSelector", "original", "clinics-_clinic-selector-ab-test")

tests[[7]] = readTestData("LHRABTest", "original", "version1")
tests[[8]] = readTestData("LHRABTest", "original", "version2")

tests[[9]] = readTestData("BlueFormTest1", "original", "variant1")
tests[[10]] = readTestData("BlueFormTest1", "original", "variant2")

tests[[11]] = readTestData("BlueFormTest2", "original", "variant1")
tests[[12]] = readTestData("BlueFormTest2", "original", "variant2")

tests[[13]] = readTestData("BlueFormTest3", "original", "variant1")
tests[[14]] = readTestData("BlueFormTest3", "original", "variant2")

tests[[15]] = readTestData("BlueFormTest4", "original", "variant1")

tests[[16]] = readTestData("BlueFormTest5", "original", "variant1")

tests[[17]] = readTestData("BlueFormTest6", "original", "variant1")

tests[[18]] = readTestData("HomepageFormTests9thDec", "original", "changectato_bookconsultation")
tests[[19]] = readTestData("HomepageFormTests9thDec", "original", "changeheadlineto_bookconsultation")
tests[[20]] = readTestData("HomepageFormTests9thDec", "original", "changeheadlineto_bookconsultation_andchangectato_b")
tests[[21]] = readTestData("HomepageFormTests9thDec", "original", "changeheadlineto_bookfreeconsultation_andchangec_1")
tests[[22]] = readTestData("HomepageFormTests9thDec", "original", "changeheadlineto_bookfreeconsultation_andchangecta")

tests[[23]] = readTestData("BookingPageFormTests9thDec", "original", "changectato_bookconsultation")
tests[[24]] = readTestData("BookingPageFormTests9thDec", "original", "changeheadlineto_bookconsultation")
tests[[25]] = readTestData("BookingPageFormTests9thDec", "original", "changeheadlineto_bookconsultation_andchangectato_b")
tests[[26]] = readTestData("BookingPageFormTests9thDec", "original", "changeheadlineto_bookfreeconsultation_andchangec_1")
tests[[27]] = readTestData("BookingPageFormTests9thDec", "original", "changeheadlineto_bookfreeconsultation_andchangecta")

tests[[28]] = readTestData("LHRFormTests9thDec", "original", "changectato_bookconsultation")
tests[[29]] = readTestData("LHRFormTests9thDec", "original", "changeheadlineto_bookconsultation")
tests[[30]] = readTestData("LHRFormTests9thDec", "original", "changeheadlineto_bookconsultation_andchangectato_b")
tests[[31]] = readTestData("LHRFormTests9thDec", "original", "changeheadlineto_bookfreeconsultation_andchangec_1")
tests[[32]] = readTestData("LHRFormTests9thDec", "original", "changeheadlineto_bookfreeconsultation_andchangecta")

tests[[33]] = readTestData("ReducedBookingPages", "original", "variant1")
tests[[34]] = readTestData("ReducedBookingPages", "original", "variant2")

tests[[35]] = readTestData("Homepage22ndRedesign", "original", "variant1")
tests[[36]] = readTestData("Homepage22ndRedesign", "original", "variant3")

numCumConversionsA <- c()
numCumConversionsB <- c()
numCumVisitsA <- c()
numCumVisitsB <- c()
testNo <- c()
testDays <- c()

for(i in 1:length(tests)){
  testDays <- append(testDays, nrow(tests[[i]]))
  testNo <- append(testNo, i)
  numCumVisitsA <- append(numCumVisitsA, tests[[i]][nrow(tests[[i]]), "numCumVisitsA"])
  numCumVisitsB <- append(numCumVisitsB, tests[[i]][nrow(tests[[i]]), "numCumVisitsB"])
  numCumConversionsA <- append(numCumConversionsA, tests[[i]][nrow(tests[[i]]), "numCumConversionsA"])
  numCumConversionsB <- append(numCumConversionsB, tests[[i]][nrow(tests[[i]]), "numCumConversionsB"])
}

testsStatistics = data.frame(testNo, testDays, conversionRateA = numCumConversionsA/numCumVisitsA, conversionRateB = numCumConversionsB/numCumVisitsB, numCumConversionsA, numCumVisitsA, numCumConversionsB, numCumVisitsB )
write.csv(testsStatistics, file = "testsStatistics.csv",row.names=TRUE)
